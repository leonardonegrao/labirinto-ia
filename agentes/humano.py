from agentes.abstrato import AgenteAbstrato
class AgentePrepostoESHumano(AgenteAbstrato):
    
    def adquirirPercepcao(self, percepcao_mundo):
        print(percepcao_mundo)
    
    def escolherProximaAcao(self):
        acao = input('Qual ação deseja? Escreva "cima", "baixo", "esquerda" ou "direita"')
        return acao