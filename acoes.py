from enum import Enum
from dataclasses import dataclass

class AcoesJogador(Enum):
    CIMA = 'cima'
    BAIXO = 'baixo'
    ESQUERDA = 'esquerda'
    DIREITA = 'direita'

@dataclass
class AcaoJogador():
    tipo: str
    parametros: tuple = tuple()

    @classmethod
    def cima(cls):
        return cls(AcoesJogador.CIMA)

    @classmethod
    def baixo(cls):
        return cls(AcoesJogador.BAIXO)

    @classmethod
    def esquerda(cls):
        return cls(AcoesJogador.ESQUERDA)

    @classmethod
    def direita(cls):
        return cls(AcoesJogador.DIREITA)