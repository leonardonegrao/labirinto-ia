import random
import copy

from regras_jogo.regras_abstratas import AbstractRegrasJogo
from regras_jogo.personagens import Personagens
from acoes import AcoesJogador

class Labirinto(AbstractRegrasJogo):
    def __init__(self, tamanho):
        self.tempo = 0
        self.board = [
            ['H','H','H','H','o','H','H','H'],
            ['H','-','-','-','-','H','H','H'],
            ['H','-','H','H','-','H','H','H'],
            ['H','-','H','H','-','-','-','-'],
            ['H','-','H','-','H','-','H','-'],
            ['H','-','-','-','H','-','H','-'],
            ['H','H','H','H','H','H','H','-'],
            ['H','H','H','H','x','-','-','-']
        ]

        self.player = [0, 4]
        self.goal = [7, 4]
        self.tentativas = 0

    def playerFileira(self):
        return self.player[0]

    def playerColuna(self):
        return self.player[1]

    def isFim(self):
        if self.player[0] == self.goal[0] and self.player[1] == self.goal[1]:
            self.terminarJogo()
            return True
        return False

    def gerarCampoVisao(self, id_agente):
        return copy.deepcopy(self.board, self.player, self.goal)

    def registrarProximaAcao(self, id_agente, acao):
        self.doNext = acao

    def atualizarEstado(self, diferencial_tempo):
        acao = self.doNext

        if acao == AcoesJogador.CIMA:
            if self.board[self.playerFileira() - 1] == '-':
                self.player[0] = self.player[0] - 1
                self.tentativas = self.tentativas + 1
            else:
                print("Jogada inválida, limite do mapa.")
        if acao == AcoesJogador.BAIXO:
            if self.board[self.playerFileira() + 1] == '-':
                self.player[0] = self.player[0] + 1
                self.tentativas = self.tentativas + 1
            else:
                print("Jogada inválida, limite do mapa.")
        if acao == AcoesJogador.ESQUERDA:
            if self.board[self.playerColuna() - 1] == '-':
                self.player[1] = self.player[1] - 1
                self.tentativas = self.tentativas + 1
            else:
                print("Jogada inválida, limite do mapa.")
        if acao == AcoesJogador.DIREITA:
            if self.board[self.playerColuna() + 1] == '-':
                self.player[1] = self.player[1] + 1
                self.tentativas = self.tentativas + 1
            else:
                print("Jogada inválida, limite do mapa.")
    
    def terminarJogo(self):
        """ Faz procedimentos de fim de jogo, como mostrar placar final,
        gravar resultados, etc...
        """
        return print(f'Fim de jogo. Movimentos: {self.tentativas}')

    def construir_jogo():
        return Labirinto()